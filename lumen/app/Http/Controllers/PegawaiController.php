<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\BusinessLayer\PegawaiBusinessLayer;
use App\DataTransferObject\PegawaiDTO;

class PegawaiController extends Controller
{
    private $pegawaiBusinessLayer;

    public function __construct()
    {
     $this->pegawaiBusinessLayer = new PegawaiBusinessLayer();   
    }

    public function ambilSemua()
    {
        $hasil = $this->pegawaiBusinessLayer->aksiAmbilSemua();

        return response()->json($hasil, $hasil['code']);
    }

    public function ambilBerdasarkanId($id)
    {
        $data = new PegawaiDTO();
        $data->setId($id);

        $hasil = $this->pegawaiBusinessLayer->aksiAmbilBerdasarkanId($data);
        return response()->json($hasil, $hasil['code']);
    }

    public function simpan(Request $request)
    {
        $data = new PegawaiDTO();
        $data->setNamaPegawai($request->input('nama'));
        $data->setAlamatPegawai($request->input('alamat'));

        $hasil = $this->pegawaiBusinessLayer->aksiSimpan($data);
        return response()->json($hasil, $hasil['code']);
    }

    public function update(Request $request, $id)
    {
        $data = new PegawaiDTO();
        $data->setId($id);
        $data->setNamaPegawai($request->input('nama'));
        $data->setAlamatPegawai($request->input('alamat'));

        $hasil = $this->pegawaiBusinessLayer->aksiSimpanBerdasarkanId($data);
        return response()->json($hasil, $hasil['code']);
    }

    public function hapus($id)
    {
        $data = new PegawaiDTO();
        $data->setId($id);

        $hasil = $this->pegawaiBusinessLayer->aksiHapus($data);
        return response()->json($hasil, $hasil['code']);
    }
}

