<?php
namespace App\BusinessLayer;
use App\DataTransferObject\PegawaiDTO;
use App\PersistanceLayer\PegawaiDAO;
use App\PresentationLayer\ResponseCreatorPresentationLayer;

class PegawaiBusinessLayer extends GenericBusinessLayer
{
    public function aksiAmbilSemua()
    {
        try {
            $data = PegawaiDAO::select('nama_pegawai', 'alamat_pegawai')
                ->orderBy('nama_pegawai', 'ASC')
                ->get();
            if (count($data) == 0) {
                $response = new ResponseCreatorPresentationLayer(404, 'Data pegawai tidak ditemukan', null, []);
                return $response->getResponse();
            }

            $response = new ResponseCreatorPresentationLayer(200, 'Data pegawai ditemukan', $data, []);
            return $response->getResponse();

        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $response = new ResponseCreatorPresentationLayer(500, 'Server sedang dalam perbaikan', null,  $errors);
        }

        return $response->getResponse();
    }

    public function aksiAmbilBerdasarkanId(PegawaiDTO $params)
    {
        try {
            $data = PegawaiDAO::find($params->getId());
            if (is_null($data)) {
                $response = new ResponseCreatorPresentationLayer(404, 'Data pegawai tidak ditemukan', null, []);
                return $response->getResponse();
            }

            $response = new ResponseCreatorPresentationLayer(200, 'Data pegawai ditemukan', $data, []);
            return $response->getResponse();

        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $response = new ResponseCreatorPresentationLayer(500, 'Server sedang dalam perbaikan', null,  $errors);
        }

        return $response->getResponse();
    }

    public function aksiSimpan(PegawaiDTO $params)
    {
        try {
            $data = new PegawaiDAO();
            $data->nama_pegawai = $params->getNamaPegawai();
            $data->alamat_pegawai = $params->getAlamatPegawai();
            $data->save();

            $hasilAkhir = [
                'id' => $data->id,
                'nama_pegawai' => $data->nama_pegawai,
                'alamat_pegawai' => $data->alamat_pegawai
            ];

            if (is_null($hasilAkhir)) {
                $response = new ResponseCreatorPresentationLayer(404, 'Gagal input data', null, []);
                return $response->getResponse();
            }

            $response = new ResponseCreatorPresentationLayer(200, 'Data berhasil disimpan', $hasilAkhir, []);
            return $response->getResponse();
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $response = new ResponseCreatorPresentationLayer(500, 'Server sedang dalam perbaikan', null,  $errors);
        }

        return $response->getResponse();
    }

    public function aksiSimpanBerdasarkanId(PegawaiDTO $params)
    {
        try {
            $data = PegawaiDAO::find($params->getId());
            $data->nama_pegawai = $params->getNamaPegawai();
            $data->alamat_pegawai = $params->getAlamatPegawai();
            $data->save();

            $hasilAkhir = [
                'id' => $data->id,
                'nama_pegawai' => $data->nama_pegawai,
                'alamat_pegawai' => $data->alamat_pegawai
            ];

            if (is_null($hasilAkhir)) {
                $response = new ResponseCreatorPresentationLayer(404, 'Gagal input data', null, []);
                return $response->getResponse();
            }

            $response = new ResponseCreatorPresentationLayer(200, 'Data berhasil disimpan', $hasilAkhir, []);
            return $response->getResponse();
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $response = new ResponseCreatorPresentationLayer(500, 'Server sedang dalam perbaikan', null,  $errors);
        }

        return $response->getResponse();
    }

    public function aksiHapus(PegawaiDTO $params)
    {
        try {
            $data = PegawaiDAO::find($params->getId());
            $data->delete();

            if (is_null($data)) {
                $response = new ResponseCreatorPresentationLayer(404, 'Data gagal dihapus', null, []);
                return $response->getResponse();
            }

            $response = new ResponseCreatorPresentationLayer(200, 'Data berhasil dihapus', null, []);
            return $response->getResponse();
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $response = new ResponseCreatorPresentationLayer(500, 'Server sedang dalam perbaikan', null,  $errors);
        }

        return $response->getResponse();
    }
}
