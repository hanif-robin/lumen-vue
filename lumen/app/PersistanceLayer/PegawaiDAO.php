<?php
namespace App\PersistanceLayer;

class PegawaiDAO extends GenericDAO
{
    protected $table = "pegawai";
    protected $primaryKey = "id";
    public $timestamps = false;
}
