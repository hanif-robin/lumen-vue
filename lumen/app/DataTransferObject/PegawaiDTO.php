<?php
namespace App\DataTransferObject;

class PegawaiDTO extends GenericDTO
{
    private $id;
    private $namaPegawai;
    private $alamatPegawai;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getNamaPegawai()
    {
        return $this->namaPegawai;
    }

    public function setNamaPegawai($namaPegawai): void
    {
        $this->namaPegawai = $namaPegawai;
    }

    public function getAlamatPegawai()
    {
        return $this->alamatPegawai;
    }

    public function setAlamatPegawai($alamatPegawai): void
    {
        $this->alamatPegawai = $alamatPegawai;
    }
}
